import React, { useCallback, useContext, useEffect, useRef, useState } from "react";
import type { NextPage } from "next";
import Router from "next/router";
import Layout from "../components/Layout";
import Footer from "../components/Footer";
import { motion, AnimatePresence } from "framer-motion";
import { experimentalStyled as styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Unstable_Grid2";
import AirplanemodeActiveIcon from "@mui/icons-material/AirplanemodeActive";
import Stack from "@mui/material/Stack";
import Alert from "@mui/material/Alert";
import Modal from "@mui/material/Modal";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import GamesIcon from "@mui/icons-material/Games";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import CheckIcon from "@mui/icons-material/Check";
import ClearIcon from "@mui/icons-material/Clear";
import Divider from "@mui/material/Divider";

const Item = styled(Paper)(({ theme }) => ({
	...theme.typography.body1,
	width: 60,
	height: 60,
	textAlign: "center",
	display: "flex",
	justifyContent: "center",
	alignItems: "center",
}));

const style = {
	position: "absolute" as "absolute",
	top: "50%",
	left: "50%",
	transform: "translate(-50%, -50%)",
	width: 400,
	bgcolor: "background.paper",
	border: "2px solid #000",
	boxShadow: 24,
	p: 4,
};

const IndexPage: NextPage = () => {
	const [gridX, setGridX] = useState<any>(10);
	const [gridY, setGridY] = useState<any>(10);
	const [moveAvailable, setMoveAvailable] = useState<any>(70);
	const [direction, setDirection] = useState<string>("NORTH");
	const [money, setMoney] = useState<number | string>(10);
	const [moveInGame, setMoveInGame] = useState<number | string>(moveAvailable);
	const [earningsAvailable, setEarningsAvailable] = useState<any[]>([]);
	const [totalEarnings, setTotalEarnings] = useState<number>(0);
	const [maxEarnings, setMaxEarnings] = useState<number>(0);
	const [gameOver, setGameOver] = useState<boolean>(false);
	const [gameWin, setGameWin] = useState<boolean>(false);
	const [isRotating, setIsRotating] = useState<boolean>(false);
	const [rotation, setRotation] = useState<number>(0);
	const [isMoving, setIsMoving] = useState<boolean>(false);
	const [moveX, setMoveX] = useState<number>(0);
	const [moveXCoordinate, setMoveXCoordinate] = useState<number>(0);
	const [moveY, setMoveY] = useState<number>(0);
	const [moveYCoordinate, setMoveYCoordinate] = useState<number>(0);
	const [error, setError] = useState<null | string>(null);
	const [openSettings, setOpenSettings] = useState<boolean>(false);
	let x = Math.ceil(12 / gridX);
	let y = Math.ceil(12 / gridY);

	function generateEarnings() {
		let earnings = [];
		for (let i = 0; i < money; i++) {
			earnings.push({
				coordinates:
					Math.floor(Math.random() * gridX - 1) +
					1 +
					"," +
					(Math.floor(Math.random() * gridY - 1) + 1),
				money: Math.floor(Math.random() * 100) + 1,
				isVisible: true,
				x: 0,
				y: 0,
			});
		}
		function getCoordinates(earnings: Array<any>) {
			for (let i = 0; i < earnings.length; i++) {
				for (let j = 0; j < i; j++) {
					if (
						earnings[i].coordinates === earnings[j].coordinates ||
						earnings[i].coordinates === "0,0"
					) {
						let random =
							(Math.floor(Math.random() * gridX - 1) + 1).toString() +
							"," +
							(Math.floor(Math.random() * gridY - 1) + 1).toString();
						if (earnings[i].coordinates !== random) {
							earnings[i].coordinates = random;
						} else {
							getCoordinates(earnings);
						}
					}
				}
			}
			return earnings;
		}
		getCoordinates(earnings);
		let currentMaxEarnings = 0;
		for (let i = 0; i < earnings.length; i++) {
			let coordinates = earnings[i].coordinates;
			earnings[i].x = parseInt(coordinates.substring(0, 1));
			earnings[i].y = parseInt(coordinates.slice(-1));
			currentMaxEarnings += earnings[i].money;
		}
		setEarningsAvailable(earnings);
		setMaxEarnings(currentMaxEarnings);
	}
	useEffect(() => {
		generateEarnings();
	}, []);
	useEffect(() => {
		generateEarnings();
	}, [gridX, gridY, money, moveAvailable]);
	useEffect(() => {
		if (moveInGame === 0 && totalEarnings != maxEarnings) {
			setGameOver(true);
		}
	}, [moveInGame, gameOver]);
	useEffect(() => {
		function sumTotalEarnings(x: number, y: number) {
			for (let i = 0; i < earningsAvailable.length; i++) {
				let currentEarnings = totalEarnings;
				let earnings: Array<any> = earningsAvailable;
				if (
					earningsAvailable[i].x === x &&
					earningsAvailable[i].y === y &&
					earnings[i].isVisible
				) {
					currentEarnings += earningsAvailable[i].money;
					setTotalEarnings(currentEarnings);
					earnings[i].isVisible = false;
					setEarningsAvailable(earnings);
					if (currentEarnings === maxEarnings) {
						setGameWin(true);
						setMoveInGame(0);
					}
				}
			}
		}
		function handleKeyDown(e: any) {
			if (isRotating === false && moveInGame > 0) {
				if (e.keyCode === 37) {
					setIsRotating(true);
					let currentRotation = rotation - 90;
					setRotation(currentRotation);
					if (direction === "EAST") {
						setDirection("NORTH");
					} else if (direction === "NORTH") {
						setDirection("WEST");
					} else if (direction === "WEST") {
						setDirection("SOUTH");
					} else if (direction === "SOUTH") {
						setDirection("EAST");
					}
				} else if (e.keyCode === 39) {
					setIsRotating(true);
					let currentRotation = rotation + 90;
					setRotation(currentRotation);
					if (direction === "EAST") {
						setDirection("SOUTH");
					} else if (direction === "SOUTH") {
						setDirection("WEST");
					} else if (direction === "WEST") {
						setDirection("NORTH");
					} else if (direction === "NORTH") {
						setDirection("EAST");
					}
				}
				setTimeout(() => {
					setIsRotating(false);
				}, 100);
			}

			if (isMoving === false && moveInGame > 0) {
				if (e.keyCode === 32) {
					setIsMoving(true);
					let currentMoveLeft: any = moveInGame;
					let currentX = moveX;
					let currentXCoordinate = moveXCoordinate;
					let currentY = moveY;
					let currentYCoordinate = moveYCoordinate;
					currentMoveLeft -= 1;
					if (direction === "EAST") {
						currentX += 60;
						currentXCoordinate += 1;
						if (currentX < gridX * 60) {
							setMoveX(currentX);
							setMoveXCoordinate(currentXCoordinate);
							sumTotalEarnings(currentXCoordinate, moveYCoordinate);
							setMoveInGame(currentMoveLeft);
						} else {
							setError("unable to move, you already reach the edge of the board");
							setTimeout(() => {
								setError(null);
							}, 3000);
						}
					} else if (direction === "WEST") {
						currentX -= 60;
						currentXCoordinate -= 1;
						if (currentX >= 0) {
							setMoveX(currentX);
							setMoveXCoordinate(currentXCoordinate);
							sumTotalEarnings(currentXCoordinate, moveYCoordinate);
							setMoveInGame(currentMoveLeft);
						} else {
							setError("unable to move, you already reach the edge of the board");
							setTimeout(() => {
								setError(null);
							}, 3000);
						}
					} else if (direction === "NORTH") {
						currentY -= 60;
						currentYCoordinate -= 1;
						if (currentY >= 0) {
							setMoveY(currentY);
							setMoveYCoordinate(currentYCoordinate);
							sumTotalEarnings(moveXCoordinate, currentYCoordinate);
							setMoveInGame(currentMoveLeft);
						} else {
							setError("unable to move, you already reach the edge of the board");
							setTimeout(() => {
								setError(null);
							}, 3000);
						}
					} else if (direction === "SOUTH") {
						currentY += 60;
						currentYCoordinate += 1;
						if (currentY < gridY * 60) {
							setMoveY(currentY);
							setMoveYCoordinate(currentYCoordinate);
							sumTotalEarnings(moveXCoordinate, currentYCoordinate);
							setMoveInGame(currentMoveLeft);
						} else {
							setError("unable to move, you already reach the edge of the board");
							setTimeout(() => {
								setError(null);
							}, 3000);
						}
					}
				}
				setTimeout(() => {
					setIsMoving(false);
				}, 100);
			}
		}
		document.addEventListener("keydown", handleKeyDown);
		return function cleanup() {
			document.removeEventListener("keydown", handleKeyDown);
		};
	}, [
		isRotating,
		isMoving,
		rotation,
		moveX,
		moveY,
		moveXCoordinate,
		moveYCoordinate,
		totalEarnings,
		earningsAvailable,
	]);

	const gameArea = [];
	for (let i = 0; i < gridY; i++) {
		gameArea.push(
			<Grid key={"y_" + i} display="flex" justifyContent="center">
				{Array.from(Array(gridX)).map((a, b) => (
					<Grid xs={x} key={"x_" + b}>
						<Item
							sx={{
								backgroundColor: (i + b) % 2 ? "gray" : "white",
							}}>
							{i === 0 && b === 0 ? (
								<motion.div
									transition={{ type: "spring" }}
									animate={{
										rotate: isRotating ? rotation : rotation,
										x: isMoving ? moveX : moveX,
										y: isMoving ? moveY : moveY,
									}}>
									<AirplanemodeActiveIcon fontSize="large" id="robot" />
								</motion.div>
							) : null}
							{earningsAvailable.map((item) => {
								if (i === item.y && b === item.x) {
									return (
										<AnimatePresence>
											{item.isVisible && (
												<motion.div key={x + "," + y}>
													{item.money}
												</motion.div>
											)}
										</AnimatePresence>
									);
								}
							})}
						</Item>
					</Grid>
				))}
			</Grid>,
		);
	}
	let alertError;
	error ? (alertError = <Alert severity="error">{error}</Alert>) : (alertError = null);
	const formatter = new Intl.NumberFormat("en-US", {
		style: "currency",
		currency: "USD",
	});
	let modalWin = (
		<Modal
			open={gameWin}
			onClose={() => Router.reload()}
			aria-labelledby="modal-modal-title"
			aria-describedby="modal-modal-description">
			<Box className="modal">
				<Typography id="modal-modal-title" variant="h6" component="h2">
					Congratulations !
				</Typography>
				<Typography id="modal-modal-description" sx={{ mt: 2 }}>
					You win this game, thank you.
				</Typography>
				<Button
					sx={{ position: "fixed", right: "2vmin", bottom: "2vmin" }}
					variant="contained"
					onClick={() => Router.reload()}>
					Restart Game
				</Button>
			</Box>
		</Modal>
	);
	let modalLose = (
		<Modal
			open={gameOver}
			onClose={() => Router.reload()}
			aria-labelledby="modal-modal-title"
			aria-describedby="modal-modal-description">
			<Box className="modal">
				<Typography id="modal-modal-title" variant="h6" component="h2">
					You Lose !
				</Typography>
				<Typography id="modal-modal-description" sx={{ mt: 2 }}>
					Game over, please try again, thank you.
				</Typography>
				<Button
					sx={{ position: "fixed", right: "2vmin", bottom: "2vmin" }}
					variant="contained"
					onClick={() => Router.reload()}>
					Restart Game
				</Button>
			</Box>
		</Modal>
	);
	const menuItem = [];
	for (let i = 5; i <= 10; i++) {
		menuItem.push(<MenuItem value={i}>{i}</MenuItem>);
	}
	const menuItemMove = [];
	for (let i = 30; i <= 100; i++) {
		menuItemMove.push(<MenuItem value={i}>{i}</MenuItem>);
	}
	return (
		<Layout>
			{modalWin}
			{modalLose}
			<Box sx={{ flexGrow: 1 }}>
				<Grid container>
					<Grid xs={4} className="indicator">
						<Stack
							sx={{ width: "100%", marginTop: "3vmin" }}
							spacing={2}
							textAlign="center"
							alignItems="center">
							<List
								sx={{
									width: "30vmin",
									bgcolor: "background.paper",
								}}>
								<ListItem>
									<ListItemAvatar>
										<Avatar>
											<GamesIcon />
										</Avatar>
									</ListItemAvatar>
									<ListItemText
										primary="Direction"
										secondary="Use keyboard arrow left and right to change direction"
									/>
								</ListItem>
								<Divider variant="inset" component="li" />
								<ListItem>
									<ListItemAvatar>
										<Avatar>
											<PlayArrowIcon />
										</Avatar>
									</ListItemAvatar>
									<ListItemText
										primary="Move"
										secondary="Use keyboard spacebar to move"
									/>
								</ListItem>
								<Divider variant="inset" component="li" />
								<ListItem>
									<ListItemAvatar>
										<Avatar>
											<CheckIcon />
										</Avatar>
									</ListItemAvatar>
									<ListItemText
										primary="Win Condition"
										secondary="If you collect all the money on the board, you will win this game"
									/>
								</ListItem>
								<Divider variant="inset" component="li" />
								<ListItem>
									<ListItemAvatar>
										<Avatar>
											<ClearIcon />
										</Avatar>
									</ListItemAvatar>
									<ListItemText
										primary="Lose Condition"
										secondary="If there is no more move available and the money still on the board, you will lose this game"
									/>
								</ListItem>
							</List>
							<Button
								sx={{ width: "30vmin" }}
								variant="contained"
								onClick={() => {
									setOpenSettings(true);
									// @ts-expect-error
									document.activeElement.blur();
								}}>
								Settings
							</Button>
							<Dialog
								disableEscapeKeyDown
								open={openSettings}
								onClose={() => setOpenSettings(false)}>
								<DialogTitle>Settings</DialogTitle>
								<DialogContent>
									<Stack
										sx={{ width: "100%" }}
										spacing={2}
										textAlign="center"
										alignItems="center">
										<Paper elevation={3}>
											<FormControl
												variant="standard"
												sx={{
													width: "30vmin",
													bgcolor: "background.paper",
												}}>
												<InputLabel id="selectGridXLabel">
													Board Column
												</InputLabel>
												<Select
													labelId="selectGridXLabel"
													id="selectGridX"
													value={gridX}
													onChange={(e) => {
														setGridX(e.target.value);
													}}
													label="Board Column">
													{menuItem}
												</Select>
											</FormControl>
										</Paper>
										<Paper elevation={3}>
											<FormControl
												variant="standard"
												sx={{
													width: "30vmin",
													bgcolor: "background.paper",
												}}>
												<InputLabel id="selectGridYLabel">
													Board Row
												</InputLabel>
												<Select
													labelId="selectGridYLabel"
													id="selectGridY"
													value={gridY}
													onChange={(e) => {
														setGridY(e.target.value);
													}}
													label="Board Row">
													{menuItem}
												</Select>
											</FormControl>
										</Paper>
										<Paper elevation={3}>
											<FormControl
												variant="standard"
												sx={{
													width: "30vmin",
													bgcolor: "background.paper",
												}}>
												<InputLabel id="selectMoneyLabel">
													Money Objective
												</InputLabel>
												<Select
													labelId="selectMoneyLabel"
													id="selectMoney"
													value={money}
													onChange={(e) => {
														setMoney(e.target.value);
													}}
													label="Money Objective">
													{menuItem}
												</Select>
											</FormControl>
										</Paper>
										<Paper elevation={3}>
											<FormControl
												variant="standard"
												sx={{
													width: "30vmin",
													bgcolor: "background.paper",
												}}>
												<InputLabel id="selectMoveLabel">
													Move Available
												</InputLabel>
												<Select
													labelId="selectMoveLabel"
													id="selectMove"
													value={moveAvailable}
													onChange={(e) => {
														setMoveAvailable(e.target.value);
														setMoveInGame(e.target.value);
													}}
													label="Move Available">
													{menuItemMove}
												</Select>
											</FormControl>
										</Paper>
									</Stack>
								</DialogContent>
								<DialogActions>
									<Button
										variant="contained"
										onClick={() => setOpenSettings(false)}>
										Submit
									</Button>
								</DialogActions>
							</Dialog>
							<Alert sx={{ width: "30vmin" }} severity="info">
								Move Available : {moveInGame}
							</Alert>
							<Alert sx={{ width: "30vmin" }} severity="success">
								Total Earnings : {formatter.format(totalEarnings)}
							</Alert>
							{alertError}
						</Stack>
					</Grid>
					<Grid xs={8} justifyContent="center" textAlign="center" className="gameArea">
						<Grid justifyContent="center" sx={{ marginTop: "3vmin" }}>
							{gameArea}
						</Grid>
					</Grid>
				</Grid>
			</Box>
		</Layout>
	);
};

export default IndexPage;

