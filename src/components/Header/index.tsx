import styles from "./Header.module.css";
import Head from "next/head";
import Grid from "@mui/material/Unstable_Grid2";

export default function Layout() {
	return (
		<Grid xs={12} id={styles.headerContainer}>
			<Grid container xs={12}>
				<Head>
					<title>Komunal Robot</title>
					<link rel="icon" href="/favicon.ico" />
				</Head>
				<Grid xs={12} display="flex" flexDirection="column" alignItems="center">
					<img src="/logo-komunal-white.png" className={styles.logo} alt="logo" />
				</Grid>
			</Grid>
		</Grid>
	);
}

