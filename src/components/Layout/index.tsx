import { ReactNode } from "react";
import Header from "../Header";
import Footer from "../Footer";
import styles from "./Layout.module.css";
import Grid from "@mui/material/Unstable_Grid2";

interface LayoutProps {
	children: ReactNode;
}

export default function Layout(props: LayoutProps) {
	const { children } = props;
	return (
		<Grid container xs={12}>
			<Header />
			<Grid id={styles.mainContainer} xs={12}>{children}</Grid>
		</Grid>
	);
}

