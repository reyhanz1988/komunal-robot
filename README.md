# Komunal Robot Game

Instructions :
1. git clone https://gitlab.com/reyhanz1988/komunal-robot
2. yarn
3. yarn dev to development
4. yarn build to build
5. yarn start to run production mode

Game :
1. Use keyboard arrow left and right to change direction
2. Use keyboard spacebar to move
3. If you collect all the money on the board, you will win this game
If there is no more move available and the money still on the board, you will lose this game

![image](https://drive.google.com/uc?export=view&id=1dj9THyq5s1CcNRb4YbGlJ2fYMNlmlIc3)